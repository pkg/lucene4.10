#!/bin/sh

set -e

VERSION=$2
FILE=$3

NEWVERSION=${VERSION}+dfsg
NEWFILE=../lucene-4.10_${NEWVERSION}.orig.tar.xz

echo "Generating ${NEWFILE} ..."
zcat $FILE \
    | tar --wildcards --delete -f - '*/tools/prettify*' \
    | xz -c > ${NEWFILE}.t

mv ${NEWFILE}.t ${NEWFILE}
